import { makeStyles } from "@material-ui/core/styles";
const useStylePanel = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    minHeight:"100vh"
  },
}));
export { useStylePanel };
