import React from "react";
import { Grid } from "@material-ui/core";
import { Label, Button } from "../../atoms";
import { useStyle } from "./style";
function LoginPanel() {
  const classes = useStyle();
  return (
    <Grid item container alignItems="center" justify="center">
      <Grid item xs={12}>
        <Label
          value="Logo"
          align="center"
          variant="h4"
          isLink={true}
          color="secondary"
          className={{ root: classes.logoRoot }}
        />
      </Grid>
      <Grid item xs={12}>
        <Label
          value="Don't Have Account?"
          align="center"
          variant="h5"
          className={{ root: classes.textRoot }}
          color="secondary"
        />
      </Grid>
      <Grid item container justify="center" alignItems="center" xs={12}>
        <Button
          disableElevation={true}
          id="submit"
          size="large"
          value="Sign Up"
          className={{ root: classes.buttonRoot }}
          variant="contained"
          color="secondary"
        />
      </Grid>
    </Grid>
  );
}
export default LoginPanel;
