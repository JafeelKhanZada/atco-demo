import React from "react";
import { Label, Input, Button } from "../../atoms";
import { Grid } from "@material-ui/core";
import { useStyle } from "./style";

function LoginForm() {
  const classes = useStyle();
  return (
    <>
      <Grid classes={{ root: classes.container }} item xs={12}>
        <Input
          id="email"
          placeholder="example@abc.com"
          helperText="Type your e-mail"
          label="Email"
          type="email"
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          id="password"
          placeholder="*********"
          helperText="Type your password"
          label="Password"
          type="password"
        />
      </Grid>
      <Grid classes={{ root: classes.container }} item xs={6}>
        <Button
          disableElevation={true}
          id="submit"
          size="large"
          value="Sign In"
          className={classes}
        />
      </Grid>
      <Grid classes={{ root: classes.container }} item xs={6}>
        <Label
          value="Forget Password?"
          variant="subtitle2"
          isLink={true}
          color="textSecondary"
        />
      </Grid>
    </>
  );
}

export default LoginForm;
