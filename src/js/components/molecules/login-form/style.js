import { makeStyles } from "@material-ui/core/styles";
const useStyle = makeStyles((theme) => ({
  root: {
    padding: "0.8rem 3rem",
    textTransform: "capitalize",
    fontSize: theme.typography.pxToRem(18),
    borderRadius: "3px",
  },
  container: {
    marginTop: "20px",
  },
}));
export { useStyle };
