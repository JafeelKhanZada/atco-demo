import React, { useState } from "react";
import Proptype from "prop-types";
import { TextField, InputAdornment, IconButton } from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
function Input(props) {
  const [visible, setVisible] = useState(false);
  const toggleVisible = () => setVisible(!visible);
  const {
    id,
    variant,
    label,
    disable,
    defaultValue,
    type,
    className,
    value,
    helperText,
    placeholder,
    change,
    error,
  } = props;
  return (
    <>
      <TextField
        fullWidth
        classes={className}
        error={error}
        defaultValue={defaultValue}
        disabled={disable}
        helperText={helperText}
        id={id}
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          endAdornment:
            type === "password" ? (
              <InputAdornment onClick={toggleVisible} position="end">
                <IconButton aria-label="toggle password visibility">
                  {visible ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ) : (
              <></>
            ),
        }}
        label={label}
        margin="normal"
        onChange={change}
        placeholder={placeholder}
        type={
          type === "password" ? (visible === false ? "password" : "text") : type
        }
        variant={variant}
        value={value}
      />
    </>
  );
}

Input.prototype = {
  id: Proptype.string,
  variant: Proptype.oneOf(["standard", "outlined", "filled"]),
  label: Proptype.string,
  disable: Proptype.bool,
  defaultValue: Proptype.string,
  type: Proptype.oneOf(["number", "text", "password"]),
  className: Proptype.object || Proptype.string,
  value: Proptype.string,
  helperText: Proptype.string,
  placeholder: Proptype.string,
  change: Proptype.func,
  error: Proptype.bool,
};
Input.defaultProps = {
  variant: "standard",
  disable: false,
  error: false,
  change: () => {},
};
export default Input;
