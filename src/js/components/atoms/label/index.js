import React from "react";
import Proptype from "prop-types";
import { Typography, Link } from "@material-ui/core";
function Label(props) {
  const {
    align,
    className,
    color,
    display,
    gutterBottom,
    isLink,
    paragraph,
    noWrap,
    to,
    variant,
    value,
  } = props;
  return (
    <>
      <Typography
        align={align}
        classes={className}
        color={color}
        display={display}
        gutterBottom={gutterBottom}
        paragraph={paragraph}
        noWrap={noWrap}
        variant={variant}
      >
        {isLink ? (
          <Link href={to} color="inherit">
            {value}
          </Link>
        ) : (
          value
        )}
      </Typography>
    </>
  );
}
Label.prototype = {
  align: Proptype.oneOf(["left", "center", "right", "justify"]),
  className: Proptype.object || Proptype.string,
  color: Proptype.oneOf([
    "primary",
    "secondary",
    "textPrimary",
    "textSecondary",
    "error",
    "link",
  ]),
  display: Proptype.oneOf(["block", "inline"]),
  gutterBottom: Proptype.bool,
  paragraph: Proptype.bool,
  noWrap: Proptype.bool,
  variant: Proptype.string,
  value: Proptype.string,
  isLink: Proptype.bool,
  to: Proptype.string,
};
Label.defaultProps = {
  display: "block",
  color: "textPrimary",
  align: "left",
};
export default Label;
