import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { Light } from "./utils/theme";
import Routes from "./utils/router";
import "./styles/index.css";
function App() {
  return (
    <>
      <ThemeProvider theme={Light}>
        <Router>
          <Switch>
            <Routes />
          </Switch>
        </Router>
      </ThemeProvider>
    </>
  );
}

export default App;
